# Use a imagem NGINX como base
FROM nginx:latest

# Copie o arquivo index.html para o diretório do NGINX
COPY index.html /usr/share/nginx/html

# Exponha a porta 80 para que a página web possa ser acessada
EXPOSE 80

# Comando de inicialização do NGINX
CMD ["nginx", "-g", "daemon off;"]

