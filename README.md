# Treinamento



## Projeto realizado a fim de aprender sobre Kubernetes.

Treinamento realizado através da Jack Experts, cujo os ensinamentos foram passados durante 1 semana (do dia 02 ao dia 06). Neste projeto utilizei o Kubernetes, Docker, Helm e MiniKube



## Programas Utilizados:

- Kubernetes
- Docker
- Minikube
- Helm



## Configuração e implementação do app

- Clone este repositório;
- Navegue para o diretório onde o repósitorio foi clonado;
- Inicie o minikube (minikube start);
- Instalar o helm no seu cluster do Minikube (helm init);
- Após os passos acima, execute o comando dentro da pasta onde o repósitorio foi clonado> helm install default ./helm/projeto
- Para verificar os status da implantação: helm list -n default.



## Acessando a aplicação

- Após implementer  o projeto, e com o minikube rodando, basta utilizar um navegador e acessar http://localhost para ver a aplicação rodando. :) 
